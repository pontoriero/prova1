package es29p129;

public class Studente extends Anagrafica{
    //sottoclasse (estesa dalla superclasse)
    //ATTRIBUTI
    private String matricola;
    private String prova1;
    //COSTRUTTORI
    public Studente () {
        super();
        matricola = "";
    }
    
    //METODI
    public String toStringStudente () {
        return super.toString() + " matricola = " + matricola;
    }
    
}
