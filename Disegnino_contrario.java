package disegnino_contrario;
import java.io.*;

public class Disegnino_contrario {
public static String readln() {

	try {
	BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
	return input.readLine();
	}	
	catch (IOException e){
	System.out.println("Attenzione errore di input");
	return "";
        }
}

    public static void main(String[] args) {
        int n;
        
        System.out.print("Inserisci il numero magico = ");
        n = Integer.parseInt(readln());
        
        for(int r=n;r>0;r--){
            for(int c=1;c<=r;c++){
                System.out.print("*");
            }
            System.out.println("");
        }
    }
    
}
