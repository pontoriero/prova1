package es29p129;

public class Anagrafica {
    //superclasse
    //ATTRIBUTI
    protected String nome;
    protected String cognome;
    protected String data_nascita;
    protected char sesso;

    //COSTRUTTORE
    public Anagrafica () {
        nome = "";
        cognome = "";
        data_nascita = "";
        sesso = ' ';
    }
    
    public Anagrafica (String nome, String cognome, String data_nascita, char sesso) {
        this.nome = nome;
        this.cognome = cognome;
        this.data_nascita = data_nascita;
        this.sesso = sesso;
    }
    
    //GET & SET
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getData_nascita() {
        return data_nascita;
    }

    public void setData_nascita(String data_nascita) {
        this.data_nascita = data_nascita;
    }

    public char getSesso() {
        return sesso;
    }

    public void setSesso(char sesso) {
        this.sesso = sesso;
    }
    
    //METODI
    public String toString () {
        return " nome = " + nome + " cognome = " + cognome + " data di nascita = " + data_nascita + " sesso = " + sesso;
    }
}