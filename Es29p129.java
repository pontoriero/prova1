package es29p129;

import java.io.*;

public class Es29p129 {

    public static String readln() {
        try {            
            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
            return input.readLine();
        } catch (IOException e) {
            System.out.println("Attenzione errore di input");
            return "";
        }
    }
    
    public static void main(String[] args) {
        Studente st = new Studente();
        st.toStringStudente();
    }
    
}
